#include <sys/time.h> /* for struct timeval */
#include <sys/types.h> /* for socket(), bind(), for open(), select() */
#include <sys/stat.h> /* for open() */
#include <fcntl.h> /* for open() */
#include <sys/socket.h> /* for socket(), bind() */
#include <arpa/inet.h> /* for htons() */
#include <string.h> /* for memset() */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <poll.h> /* for poll() */
#include <getopt.h> /* for getopt_long() */
#include "header.h"
#include "zlib-1.2.8/zlib.h" /* for crc32() */

// ./receiver port

int fd = 1; /* file descriptor, stdout by default */
char* hostname = "::";
char* filename = NULL;
int lastSeqNum = -1; /* Last sequence number received in sequence */
unsigned short int window = WINDOW_SIZE_MAX/2;
int fullSlots = 0;
int emptySlots = WINDOW_SIZE_MAX;
unsigned int receivedPackets = 0;
unsigned int sentAck = 0;
int seriesOfSN = 0;

packet_t* makeAck() {
	packet_t* acknowledgment = (packet_t*)calloc(sizeof(packet_t),1);
	acknowledgment->header = headerToInt(PTYPE_ACK, window - fullSlots, lastSeqNum+1, 0);
	unsigned int crc = crc32(0, NULL, 0);
	crc = crc32(crc, (Bytef*)&(acknowledgment->header), sizeof(acknowledgment->header));
	crc = crc32(crc, (Bytef*)acknowledgment->buffer, sizeof(acknowledgment->buffer));
	acknowledgment->CRC = crc;
	return acknowledgment;
}

void writePacketInFile(packet_t* paquet) {
	header_t head = intToHeader(paquet->header);
	printf("header length = %d\n", head.length);
	printf("header type = %d\n", head.type);
	printf("header window = %d\n", head.window);
	printf("header seqNum = %d\n", head.seqNum);
	if (write(fd, paquet->buffer, head.length) == -1) {
		fprintf(stderr, "Erreur de write()\n");
	}
}

void deletePacket(packet_t* paquet) {
	free(paquet);
	paquet=NULL;
}

int main(int argc, char* argv[]) {
	
	int c;
	int option_index = 0;
	static struct option long_options[] = {
		{"file", required_argument, NULL, 'f'},
		{0, 0, 0, 0},
	};
	while ( (c = getopt_long(argc, argv, "f:", long_options, &option_index)) != -1) {
		switch (c) {
			case 'f' :
				if (strcmp("file", long_options[option_index].name) == 0) {
					filename = optarg;
				}
				break;
			default :
				fprintf(stderr, "Unhandled option -%c", c);
				break;
		}
	}
	
	hostname = argv[argc-2];
	unsigned short localPort = atoi(argv[argc-1]);
	int sock; /* Socket descriptor */
	struct sockaddr_in6 localAddr; /* Local address */
	
	/* Ouverture de la socket */
	sock = socket(PF_INET6, SOCK_DGRAM, 0);
	
	/* Ouverture d'un fichier en ecriture */
	if (filename != NULL) {
		fd = open(filename, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
		if (fd == -1) {
			fprintf(stderr, "erreur d'open()\n");
			return EXIT_FAILURE;
		}
	}
	/* Initialisation de la structure adresse locale */
	memset(&localAddr, 0, sizeof(localAddr));
	localAddr.sin6_family = AF_INET6;
	localAddr.sin6_addr = in6addr_any;
	localAddr.sin6_port = htons(localPort);
	
	/* Relie la socket a l'adresse locale sweetie */
	bind(sock, (struct sockaddr*)&localAddr, sizeof(localAddr));
	
	/* Tableau de pointeurs vers les packet_t en attendant */
	packet_t** paquets = (packet_t**)malloc(WINDOW_SIZE_MAX*sizeof(packet_t*));
	
	/* Initialisation des informations pour le poll des paquets recus */
	struct pollfd fdsRECVFROM;
	fdsRECVFROM.fd = sock;
	fdsRECVFROM.events = POLLIN;
	
	/* Initialisation des informations pour le poll des ack envoyes */
	struct pollfd fdsSENDTO;
	fdsSENDTO.fd = sock;
	fdsSENDTO.events = POLLOUT;
	
	/* Boucle jusqu'a la reception du dernier paquet */
	int notFinish = 1;
	while ( notFinish ) {
		/* Initialisation du paquet a recevoir */
		packet_t* paquet = NULL;
		
		/* Initialisation de la structure adresse distante */
		struct sockaddr_in6 senderAddr;
		unsigned int senderAddrLen = sizeof(senderAddr);
		
		/* attente de paquets par poll sur la socket */
		int pollretval;
		pollretval = poll(&fdsRECVFROM, 1, 1000);
		if (pollretval == -1) {
			fprintf(stderr, "Error of poll() for recvfrom...\n");
		}
		else if (pollretval && fdsRECVFROM.revents==POLLIN) {
			paquet = (packet_t*)malloc(sizeof(packet_t));
			/* Reception de l'information mon ptit */
			recvfrom(sock, paquet, PACKET_SIZE, 0, (struct sockaddr*)&senderAddr, &senderAddrLen);
		}
		else {
			/* Nothing was received */
			fprintf(stderr, "timeout waiting for receiving packet\n");
			paquet = NULL;
		}

		
		/* Verification du paquet */
		if (paquet == NULL) {
			//fprintf(stderr, "A packet received had a problem.\n");
		}
		else {
			header_t head = intToHeader(paquet->header);
			unsigned int crc = crc32(0, NULL, 0);
			crc = crc32(crc, (Bytef*)&(paquet->header), sizeof(paquet->header));
			crc = crc32(crc, (Bytef*)paquet->buffer, sizeof(paquet->buffer));
			if (head.type != PTYPE_DATA && head.window != 0 && head.length <= 512 && crc != paquet->CRC) {
				/* Reception du mauvais type. */
				fprintf(stderr, "Un des paquets recus ne correspond pas aux specifications.\n");
				deletePacket(paquet);
			}
			else if ( (window - fullSlots == 1) && (head.seqNum != (lastSeqNum+1)) ) {
				/* Il ne reste plus qu'une place dans le buffer
				 * et le paquet reçu n'est pas celui qui permettrait
				 * de decaler la window.
				 */
				fprintf(stderr, "Le buffer est en attente du paquet en sequence...\n");
				deletePacket(paquet);
			}
			else if (head.seqNum > lastSeqNum) {
				/* Mise du paquet dans le buffer */
				printf("Mise en paquet du buffer %d\n", head.seqNum);
				paquets[head.seqNum % (WINDOW_SIZE_MAX-1)] = paquet;
				fullSlots++;
				emptySlots--;
				receivedPackets++;
			}
			else {
				deletePacket(paquet);
			}
		}
		
		if (sentAck < receivedPackets) {
			/* Verification possibilite d'ecrire l'ack dans la socket */
			pollretval = poll(&fdsSENDTO, 1, 1000);
			if (pollretval == -1) {
				fprintf(stderr, "Error of poll() for sendto...\n");
			}
			else if (pollretval && fdsSENDTO.revents==POLLOUT) {
				/* There is place to write an ack in the socket */
			
				/* Initialisation de l'acquittement */
				packet_t* ack = makeAck();
				
				header_t ackHeader = intToHeader(ack->header); 
				printf("ack %d envoye\n", ackHeader.seqNum);
				/* Envoi de l'acquittement relatif au paquet recu */
				if (paquet != NULL && sendto(sock, ack, PACKET_SIZE, 0, (struct sockaddr*)&senderAddr, sizeof(senderAddr)) != PACKET_SIZE) {
					fprintf(stderr, "sendto() n'a pas envoye un acknowledgment de la taille prevue\n");
				}
				else {
					sentAck++;
				}
				/* Suppression de l'acquittement envoye */
				deletePacket(ack);
			}
			else {
				/* No ack can be written */
				fprintf(stderr, "timeout can't write anymore ack in the socket\n");
			}
		}
		
		/* Ecriture de tous les paquets qui sont en sequence 
		 * meme ceux qui etaient arrives hors-sequence.
 		 */
		int i = 0;
		while (fullSlots > 0 && i < WINDOW_SIZE_MAX) {
			if (paquets[i] != NULL) {
				packet_t* p = paquets[i];
				header_t h = intToHeader(p->header);
				printf("hseqnum= %d\n", h.seqNum);
				if (h.seqNum == lastSeqNum+1 || (lastSeqNum==255 && h.seqNum==0)) {
					printf("Ecriture du paquet %d\n", h.seqNum);
					writePacketInFile(p);
					if (h.length != PAYLOAD_SIZE) {
						notFinish = 0;
					}
					fullSlots--;
					emptySlots++;
					if (lastSeqNum == 255) {
						printf("Je rencontre le 255eme ! retour a 0\n");
						seriesOfSN++;
						sentAck = -1;
						receivedPackets = 0;
					}
					lastSeqNum = h.seqNum;
					deletePacket(p);
					paquets[i] = NULL;
					i=-1;
					 /* Je repars de 0 s'il y avait un paquet a ecrire*/
				}
			}
			i++;
		}
		
	}
	
	free(paquets);
	
	/* Fermeture du fichier */
	close(fd);
	
	/* Fermeture de la socket */
	close(sock);
	
	return EXIT_SUCCESS;
}
