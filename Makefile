CC=gcc
SRC=$(wildcard *.c)
CFLAGS= -Wall -Werror -g
EXEC= sender receiver

all: $(EXEC)

receiver: receiver.c header.o
	@$(CC) $^ -o $@ -lz

sender: sender.c header.o 
	@$(CC) $^ -o $@ -lz

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean mrproper

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)

