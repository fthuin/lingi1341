#include <sys/types.h> /* for socket(), sendto(), recvfrom(), fd open() */
#include <sys/stat.h> /* for fd open() */
#include <fcntl.h> /* for fd open() */
#include <sys/socket.h> /* for socket(), sendto(), recvfrom() */
#include <arpa/inet.h> /* for htons(), inet_pton() */
#include <string.h> /* for memset() */
#include <stdio.h>
#include <stdlib.h>
#include <poll.h> /* for poll() */
#include <unistd.h> /* for close(), for usleep() */
#include <getopt.h> /* for getopt_long() */
#include "header.h"
#include "zlib-1.2.8/zlib.h" /* for crc32() */

int fd = 0; /* File descriptor, stdin by default */
char* filename = NULL; /* name of the input file to send */
unsigned short seqNum = 0; /* last sequence number used to send */
unsigned short sentPackets = 0;
unsigned int sber = 0; /* Transmission error ratio per thousand */
unsigned int splr = 0; /* Packet loss ratio in percentage */
unsigned int delay = 0; /* delay before transmitting each packet in milliseconds */
int lastSeqNum = -2; /* Only changes when last packet is sent */
int lastSeqNumAck = -1;
int nbrReceivedAck = 0;
int receiverWindow = 1;
int nSeriesOfSN = 0;

void usage() {
	printf("Usage: \n");
	printf("./sender [--file filename] [--sber x] [--splr x] [--delay d] hostname port\n");
	printf("filename : name of the file to send, if empty then read the standard input\n");
	printf("sber : ...\n");
	printf("hostname : receiver's IPv6 or name\n");
	printf("port : the port of the application of the receiver\n");
	exit(1);
}

packet_t* getSpecificPacketFromFile(int fd, int pseqNum) {
	packet_t* paquet = (packet_t*)calloc(sizeof(packet_t), 1);
	unsigned int currentOffset = lseek(fd, 0, SEEK_CUR);
	lseek(fd, (nSeriesOfSN*256*512)+(pseqNum)*512, SEEK_SET);
	unsigned int bytesLus;
	bytesLus = read(fd, paquet->buffer, PAYLOAD_SIZE);
	paquet->header = header(headerConstruct(PTYPE_DATA, 0, pseqNum, htons(bytesLus)));
	uint32_t crc = crc32(0, NULL, 0);
	crc = crc32(crc, (Bytef*)&(paquet->header), sizeof(paquet->header));
	crc = crc32(crc, (Bytef*)paquet->buffer, sizeof(paquet->buffer));
	paquet->CRC = htonl(crc);
	if (random()%1000 < sber) {
 		paquet->buffer[0] ^= 0xff;
	}
	header_t head = intToHeader(paquet->header);
	lseek(fd, currentOffset, SEEK_SET);
	return paquet;
}

packet_t* getPacketFromFile(int fd) {
	packet_t* paquet = (packet_t*)calloc(sizeof(packet_t),1);
	if (paquet == NULL) {
		fprintf(stderr, "malloc() error\n");
		exit(1);
	}
	unsigned int bytesLus;
	bytesLus = read(fd, paquet->buffer, PAYLOAD_SIZE);
	if (bytesLus == -1) {
		fprintf(stderr, "read() error on input file\n");
	}
	paquet->header = header(headerConstruct(PTYPE_DATA, 0, seqNum, bytesLus));
	paquet->CRC = 0; /* TODO */
	header_t head = intToHeader(paquet->header);
	return paquet;
}

void deletePacket(packet_t* paquet) {
	free(paquet);
	paquet=NULL;
}

int main(int argc, char* argv[]) {
	if (argc < 3) {
		usage();
	}
	int c; /* Caractere de l'option en input */
	int option_index = 0;
	static struct option long_options[] = {
		{"file", required_argument, NULL, 'f'},
		{"sber", required_argument, NULL, 's'},
		{"splr", required_argument, NULL, 'p'},
		{"delay", required_argument, NULL, 'd'},
		{0, 0, 0, 0}
	};
	
	/* Recuperation des arguments passes en ligne de commande */
	while ( (c = getopt_long(argc, argv, "f:s:p:d", long_options, &option_index)) != -1 ) {
		switch (c) {
			case 'f' :
				if (strcmp("file", long_options[option_index].name) == 0) {
					filename = optarg;
				}
				break;
			case 's' :
				if (strcmp("sber", long_options[option_index].name) == 0) {
					sber = atoi(optarg);
				}
				break;
			case 'p' :
				if (strcmp("splr", long_options[option_index].name) == 0) {
					splr = atoi(optarg);
				}
				break;
			case 'd' :
				if (strcmp("delay", long_options[option_index].name) == 0) {
					delay = atoi(optarg);
				}
				break;
			default :
				fprintf(stderr, "Unhandled option -%c", c);
				break;
		}
	}

	int sock; /* Socket descriptor - comme un file descriptor */
	
	/* Ouverture d'une socket et check error */
	if ((sock = socket(AF_INET6, SOCK_DGRAM, 0)) == -1) {
		fprintf(stderr, "Erreur d'ouverture de la socket\n");
		return EXIT_FAILURE;
	}
	
	/* Recuperation des donnees du receiver */	
	char* IPv6 = argv[argc-2];
	unsigned short receiverPort = atoi(argv[argc-1]);
	
	/* Ouverture du fichier */
	if (filename != NULL) {
		fd = open(filename, O_RDONLY);
		if (fd == -1) {
			fprintf(stderr, "Erreur de open()\n");
			exit(1);
		}
	}
	/* Initialisation des données du sender lui-meme. */
	//struct sockaddr_in6* receiverAddress = (struct sockaddr_in6*)calloc(sizeof(struct sockaddr_in6), 1);
	struct sockaddr_in6 receiverAddress;
	memset(&receiverAddress, 0, sizeof(receiverAddress));
	receiverAddress.sin6_family = AF_INET6;
	receiverAddress.sin6_port = htons(receiverPort);
	inet_pton(AF_INET6, IPv6, &(receiverAddress.sin6_addr.s6_addr));
	
	/* Initialisation des informations pour le poll d'ecriture sur socket */
	struct pollfd fdsSENDTO;
	fdsSENDTO.fd = sock;
	fdsSENDTO.events = POLLOUT;
	
	/* Initialisation des informations pour le poll de lecture sur socket */
	struct pollfd fdsRECVFROM;
	fdsRECVFROM.fd = sock;
	fdsRECVFROM.events = POLLIN;
	
	/* Boucle jusqu'a l'acquittement du dernier paquet */
	int notFinish = 1;
	while (notFinish) {
		/* Verification possibilite d'ecrire dans la socket */
		int pollretval;
		pollretval = poll(&fdsSENDTO, 1, 500);
		if (pollretval == -1) {
			fprintf(stderr, "Error of poll() for sendto...");
		}
		else if (pollretval && fdsSENDTO.revents==POLLOUT) {
			/* There is place to write in the socket ! :D */
/* Initialisation du paquet a envoyer */
			packet_t* paquet;
			paquet=getSpecificPacketFromFile(fd, seqNum);
			header_t header = intToHeader(paquet->header);
			
			//usleep(delay*1000);
			/* Envoi du paquet */
			if (random()%100 > splr && sendto(sock, paquet, PACKET_SIZE, 0, (struct sockaddr*) &receiverAddress, sizeof(receiverAddress)) != PACKET_SIZE ) {
				fprintf(stderr, "sendto() n'a pas envoye un paquet de la taille prevue\n");
			}
			else {
				if (seqNum == 255) {
					seqNum = 0;
					nSeriesOfSN++;
				}
				else {
					seqNum++;
				}
			
				if (ntohs(header.length) != PAYLOAD_SIZE) {
					lastSeqNum = header.seqNum;
				}
				sentPackets++;
				/* Reussite envoi - Suppression du paquet envoye */
				deletePacket(paquet);
			}
		}
		else {
			/* Nothing can be written */
			fprintf(stderr, "timeout can't write anymore in the socket to send ");
		}
		
		/* Initialisation de la structure pour connaitre qui nous envoie */
		struct sockaddr_in6 otherPartAddr;
		unsigned int otherPartAddrSize = sizeof(otherPartAddr);
		
		if (nbrReceivedAck < sentPackets) {
			/* Attente d'acknowledgment par poll sur la socket */
			pollretval = poll(&fdsRECVFROM, 1, 500);
			if (pollretval == -1) {
				fprintf(stderr, "Error of poll() for recvfrom\n");
			}
			else if (pollretval && fdsRECVFROM.revents == POLLIN) {
				/* Initialisation de l'acknowledgment a receptionner */
				packet_t* ack = (packet_t*)malloc(sizeof(packet_t));

				/* Reception d'un paquet acknowledgement */
				if (recvfrom(sock, ack, PACKET_SIZE, 0, (struct sockaddr*)&otherPartAddr, &otherPartAddrSize) != PACKET_SIZE) {
					fprintf(stderr, "recvfrom() n'a pas recu un acknowledgment de la taille prevue.\n");
				}
				else {
					uint32_t crc = crc32(0, NULL, 0);
					crc = crc32(crc, (Bytef*)&(ack->header), sizeof(ack->header));
					crc = crc32(crc, (Bytef*) ack->buffer, sizeof(ack->buffer));
					if (crc = ntohl(ack->CRC) ) {
						nbrReceivedAck++;
						header_t ackHeader = intToHeader(ack->header);
						// TODO : Verifier le CRC de l'ack 
						if (lastSeqNumAck < ackHeader.seqNum) {
							lastSeqNumAck = ackHeader.seqNum;
						}
						receiverWindow = ackHeader.window;
					}
					else {
						fprintf(stderr, "Bad CRC on ack\n");
					}
				}
				/* Suppression de l'acknowledgment */
				deletePacket(ack);
			}
			else {
				/* Nothing was received */
				if (lastSeqNumAck == 255) {
					lastSeqNumAck=-1;
				}
				seqNum = lastSeqNumAck+1;
				//sentPackets = lastSeqNumAck;
				fprintf(stderr, "timeout waiting for acknowledgment\n");
			}
		}

		/* Verification de la reception du dernier ACK */
		if (lastSeqNumAck == lastSeqNum) {
			notFinish = 0;
		}
	}
	
	
	//free(receiverAddress);
	
	close(sock);
	
	close(fd);
	
	return EXIT_SUCCESS;
}

