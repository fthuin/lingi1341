#include <stdio.h> /* for printf() */
#include <stdlib.h> /* for EXIT_SUCCESS, EXIT_FAILURE */
#include <stdint.h> /* for uint16_t and uint32_t */
//#include <zlib.h>

/* Paquet de type DATA */
#define PTYPE_DATA 1
/* Paquet de type ACK */
#define PTYPE_ACK 2
/* Taille du payload en bytes */
#define PAYLOAD_SIZE 512
#define PACKET_SIZE 520
#define WINDOW_SIZE_MAX 32


typedef struct header {
	uint16_t type;
	uint16_t window;
	uint16_t seqNum;
	uint16_t length;
} header_t;

typedef struct packet {
	uint32_t header;
	char buffer[PAYLOAD_SIZE];
	uint32_t CRC;
} packet_t;

//unsigned int crcConstruct(packet_t* paquet);

header_t headerConstruct(uint16_t typ, uint16_t wind, uint16_t seqN, uint16_t len);

uint32_t headerToInt(uint16_t type, uint16_t windowSize, uint16_t seqNum, uint16_t length);

uint32_t header(header_t h);

header_t intToHeader(uint32_t i);
