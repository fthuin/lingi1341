#include "header.h"

header_t headerConstruct(uint16_t typ, uint16_t wind, uint16_t seqN, uint16_t len) {
	header_t h;
	h.type = typ;
	h.window = wind;
	h.seqNum = seqN;
	h.length = len;
	return h;
}

uint32_t headerToInt(uint16_t type, uint16_t windowSize, uint16_t seqNum, uint16_t length) {
	uint32_t i = 0;
	i |= (length << 16);
	i |= (seqNum << 8);
	if (type == PTYPE_DATA) {
		i |= (PTYPE_DATA << 5);
	}
	else if (type == PTYPE_ACK) {
		i |= (PTYPE_ACK << 5);
	}
	
	i |= windowSize;
	/* if (type == PTYPE_DATA) {
		i |= (1 << 29);
	}
	else if (type == PTYPE_ACK) {
		i |= (1 << 30);
	}
	i |= (windowSize << 24);
	i |= (seqNum << 16);
	i |= length; */
	
	return i;
}

/* Remplit un entier de 32 bits les bits par les bits utiles de la structure */
uint32_t header(header_t h) {
	return headerToInt(h.type, h.window, h.seqNum, h.length);
}

/* Cree une structure  */
header_t intToHeader(uint32_t i) {
	uint16_t length, seqNum, type, window;

	length = 65535 & (i >> 16);
	seqNum = 255 & (i >> 8);
	type = 7 & (i >> 5);
	window = 31 & i;
	

	/* type = 7 & (i >> 29);
	window = 31 & (i >> 24);
	seqNum = 255 & (i >> 16);
	length = 65535 & i; */
	return headerConstruct(type, window, seqNum, length);
}

